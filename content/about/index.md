---
title: "À propos"
date: 2021-12-06 23:26:00
---

L'Atelier de Formologie Appliquée, créé en novembre 2000, est un groupe d'experts des formes qui s'appuie sur une démarche innovante et transgressive, au croisement des sciences et des arts.


- Framagit: [afa](https://framagit.org/afa)
- Vimeo : [Al Emrof](https://vimeo.com/user159837018)
