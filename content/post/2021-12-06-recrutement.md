---
title: "Campagne de recrutement 2022"
date: 2021-12-06T22:14:16+01:00
---

L'Atelier de Formologie Appliquée lance une campagne de recrutement pour une mise à jour du projet "**La Forme - Rework**".

Avec une infinie sagesse, le directeur Al Emrof a estimé qu'il était temps d'agrandir son équipe car les ramifications et les enjeux actuels amènent de nouvelles questions, tensions et perspectives. La mue de l'Atelier a déjà été opérée puisqu'il agit déjà en parallèle des choses conscientes. Il est temps néanmoins que les choses parlent et puissent contribuer au développement des sociétés hybrides, aux lisières des classifications.

Le jeune Al Emrof, âgé de 21 ans, a besoin de vous. Les Formes invisibles sont muettes alors reprenons le travail.

Cette lettre est destinée aux seuls membres permanents et leurs collaborateurs. La mission s'écrit en leur nom. S'ensuivra des procédures et méthodes automatiques qui jalonneront ce parcours de la plus haute importance.

Si vous souhaitez ouvrir la porte, la fiche de poste s'écrira au fur et à mesure. De plus amples informations vous serons communiquées ultérieurement.

**Signature** : Al Emrof, chercheur de l'absolu.

**Préambule** : Les affaires du monde importent; Les choses ne se présentent pas dans le bon alignement. Des gens souffrent. Trop de vies sont fragilisées et d'immenses mécanismes d'agressions et de cécité sont à l’œuvre. Il est temps de ne pas sombrer.


**Source** : [lettre de recrutement (pad)](https://semestriel.framapad.org/p/atelier-formologie-appliquee-recrutement-2022-9r99?lang=fr)



{{< vimeo 653893704 >}}

